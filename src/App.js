import React, {Component} from 'react'
import classes from './App.module.css';
import Person from './Person/Person'

class App extends Component {
    state = {
        persons: [
            {id: 1, name: "Manh", age: 25},
            {id: 2, name: "Manu", age: 22}
        ],
        showPerson: false
    };

    changeNameHandler = (event, personId) => {
        const persons = this.state.persons;
        let personIndex = persons.findIndex(person => person.id === personId);
        if (personIndex !== -1) {
            persons[personIndex].name = event.target.value;
        }

        this.setState({persons: persons});
    };

    deletePersonHandler = (personIndex) => {
        const persons = [...this.state.persons];
        persons.splice(personIndex, 1);
        this.setState({persons: persons})
    };

    togglePersonsHandler = () => {
        const doesShowPerson = this.state.showPerson;
        this.setState({
            showPerson: !doesShowPerson
        })
    };

    render() {
        const style = {
            backgroundColor: 'white',
            font: 'inherit',
            border: '1px solid blue',
            padding: '8px',
            cursor: 'pointer',
            marginBottom: '20px'
        };

        let personBlock = null;
        if (this.state.showPerson) {
            personBlock = (
                <div>
                    {
                        this.state.persons.map((person, index) => {
                            return <Person clickDeletePerson={() => this.deletePersonHandler(index)}
                                    onChangeName={(event) => this.changeNameHandler(event, person.id)}
                                    name={person.name} age={person.age} key={person.id}></Person>
                        })
                    }
                </div>
            )
        }

        const assignedClasses = [];
        if (this.state.persons.length <= 2) {
            assignedClasses.push(classes.red)
        }
        if (this.state.persons.length <= 1) {
            assignedClasses.push(classes.bold)
        }
        return (
            <div className={classes.App}>
                <h1>This is first react app</h1>
                <p className={assignedClasses.join(' ')}>Hello world</p>
                <button onClick={this.togglePersonsHandler.bind(this)}>Show us!</button>
                {personBlock}
            </div>
        );
    }
}

export default App;
