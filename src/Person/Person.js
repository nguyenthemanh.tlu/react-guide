import React from 'react'
import PersonClasses from './Person.module.css'


const person = (props) => {
    return (
        <div className={PersonClasses.Person}>
            <button onClick={props.clickDeletePerson}>Delete me !</button>
            <h1>My name is {props.name} and I'm {props.age} years old</h1>
            <input type="text" onChange={props.onChangeName} value={props.name}/>
        </div>
    )
};


export default person;